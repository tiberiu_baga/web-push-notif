Setting up the web-push-notifications Quickstart
===================================

Be advised: The files contain the public keys needed (js) files, and a key for sending the push notif, in the example.
Do not share that key with anyone!

Contents:
- Firebase-messaging-sw.js -> file needed for receiving web-push-notifications in the background
        - Index.php -> an example file that shows how the web-push notifications work, and how to integrate
        - Main.css (style file needed for example)
        - Manifest.json - File needed for some browsers that will use this to enable push notifications.
        - README.md - basic documentation
        - Sending-push-notifications-example.txt -> a POST example on how to trigger(send) the web-push-notifications
        - Web-push-notif.js -> basic js functionality to be included on desired page, also contains functions to be edited (see below)

Quickstart instruction
- Copy the firebase-messaging-sw.js to the webpage document root (public)
- Copy the manifest.json to the webpage document root
- Copy the web-push-notif.js to the webpage js folder (or document root )
- Include in the page where you wish to ask for web-push permissions the following (see index.php for example):
          - <script src="https://www.gstatic.com/firebasejs/7.5.0/firebase-app.js"></script>
          - <script src="https://www.gstatic.com/firebasejs/7.5.0/firebase-auth.js"></script>
          - <script src="https://www.gstatic.com/firebasejs/7.5.0/firebase-messaging.js"></script>
          - <script src="web-push-notif.js"></script>
- Modify the following functions from web-push-notif.js:
          - sendTokenToServer(refreshedToken) -> this is where the token is sent and saved on a specific server (needs to be implemented)
          - Other functionality if needed (How token is requested, do not display on page -> do we need to deleteToken , etc.)


