var pageURL = 'https://www.orange.ro/?test';
var _cio = _cio || [];
    (function() {
        var a,b,c;a=function(f){return function(){_cio.push([f].
        concat(Array.prototype.slice.call(arguments,0)))}};b=["load","identify",
        "sidentify","track","page"];for(c=0;c<b.length;c++){_cio[b[c]]=a(b[c])};
        var t = document.createElement('script'),
            s = document.getElementsByTagName('script')[0];
        t.async = true;
        t.id    = 'cio-tracker';
        t.setAttribute('data-site-id', 'ae813d3867e6d437545b');
        t.src = 'https://assets.customer.io/assets/track.js';
        s.parentNode.insertBefore(t, s);
    })();

var nrpg = 3;

var fcmUserToken = '';
var fcmNotifPermissionRequested = localStorage.getItem('fcmNotifPermissionRequested');
var firebaseConfig = {
  apiKey: "AIzaSyB_glG1v9ctpnbwUgy1BV_ASUc03MEyPQQ",
  authDomain: "push-notifications-c0fc0.firebaseapp.com",
  databaseURL: "https://push-notifications-c0fc0.firebaseio.com",
  projectId: "push-notifications-c0fc0",
  storageBucket: "push-notifications-c0fc0.appspot.com",
  messagingSenderId: "819391863621",
  appId: "1:819391863621:web:76a324a2d9e26dbc5ebc2c"
};
firebase.initializeApp(firebaseConfig);
var messaging = firebase.messaging();
messaging.usePublicVapidKey('BHsJV2b0gGXNFbm_dhdXu8P-vDqi6615jXuzzlYOit66oIEiTIY_CFop68DBi8vTMoL3DQNAJBwIQRqFMdq4YHY');

messaging.onMessage(function(payload){
 var myNotification = new Notification(payload.notification.title, payload.notification);
});

function resetUI() {
  messaging.getToken().then(function(currentToken) {
    if (currentToken) {
      console.log('Instance ID token available: ' +  currentToken);
      fcmUserToken = currentToken;
      messaging.subscribeToTopic(currentToken, 'allTest').then(function(response) {
          console.log('Successfully subscribed to topic:', response);
      })
      .catch(function(error) {
        console.log('Error subscribing to topic:', error);
      });

      if (fcmNotifPermissionRequested === '1') {
        alert('INNNNNNN');
        _cio.identify({
          id: fcmUserToken.substring(0, 128),
          email: '',
          created_at: Math.floor(Date.now() / 1000),
          pushId: fcmUserToken
        });
        sessionStorage.setItem('fcmNotifPermissionRequested', 0);
        _cio.track("subscribed-to-web-push-notifications");
      } else {
        _cio.identify({
          id: fcmUserToken.substring(0, 128),
          email: '',
          pushId: fcmUserToken
        });
      }
      _cio.track("page", pageURL);
    } else {
      sessionStorage.setItem('fcmNotifPermissionRequested', 1);
      console.log('No Instance ID token available.');
      if (nrpg > 1 && pageURL.indexOf('aplicatii/extensie-chrome') == -1) {
        requestPermission();
      }
    }
  }).catch(function(err){
    // if( err.code.indexOf('messaging/notifications-blocked') != -1 ) {
    //   console.log('Notification were blocked.');
    // } else {
      console.log('An error occurred while retrieving token. ', err);
    // }
  });
}

function requestPermission() {
  console.log('Requesting permission...');
  Notification.requestPermission().then(function(permission){
    if (permission === 'granted') {
      alert('Permission Granted!!!!');
      sessionStorage.setItem('fcmNotifPermissionRequested', 1);
      console.log('Notification permission granted.');
      resetUI();
    } else {
      console.log('Unable to get permission to notify.');
    }
  });
}

// messaging.onTokenRefresh(function(){
//   messaging.getToken().then(function(refreshedToken){
//     console.log('Token refreshed.');
//     resetUI();
//   }).catch(function(err){
//     console.log('Unable to retrieve refreshed token ', err);
//   });
// });

resetUI();
//   // alert(fcmUserToken);
//   // if (nrpg > 1 && pageURL.indexOf('aplicatii/extensie-chrome') == -1) {
//   //   requestPermission();
//   // }

