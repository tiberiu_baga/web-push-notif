<script>
var fcmUserToken = '';
var firebaseConfig = {
  apiKey: "AIzaSyB_glG1v9ctpnbwUgy1BV_ASUc03MEyPQQ",
  authDomain: "push-notifications-c0fc0.firebaseapp.com",
  databaseURL: "https://push-notifications-c0fc0.firebaseio.com",
  projectId: "push-notifications-c0fc0",
  storageBucket: "push-notifications-c0fc0.appspot.com",
  messagingSenderId: "819391863621",
  appId: "1:819391863621:web:76a324a2d9e26dbc5ebc2c"
};
firebase.initializeApp(firebaseConfig);
var messaging = firebase.messaging();
messaging.usePublicVapidKey('BHsJV2b0gGXNFbm_dhdXu8P-vDqi6615jXuzzlYOit66oIEiTIY_CFop68DBi8vTMoL3DQNAJBwIQRqFMdq4YHY');

messaging.onMessage(function(payload){
 var myNotification = new Notification(payload.notification.title, payload.notification);
});

function resetUI() {
  messaging.getToken().then(function(currentToken) {
    if (currentToken) {
      console.log('Instance ID token available: ' +  currentToken);
      fcmUserToken = currentToken;
    } else {
      console.log('No Instance ID token available. Request permission to generate one.');
      requestPermission();
    }
  }).catch(function(err){
    console.log('An error occurred while retrieving token. ', err);
  });
}

function requestPermission() {
  console.log('Requesting permission...');
  Notification.requestPermission().then(function(permission){
    if (permission === 'granted') {
      console.log('Notification permission granted.');
      resetUI();
    } else {
      console.log('Unable to get permission to notify.');
    }
  });
}

function getFcmToken() {
  return fcmUserToken;
}

function deleteToken() {
  messaging.getToken().then(function(currentToken){
    messaging.deleteToken(currentToken).then(function(){
      console.log('Token deleted.');
      resetUI();
    }).catch(function(err){
      console.log('Unable to delete token. ', err);
    });
  }).catch(function(err){
    console.log('Error retrieving Instance ID token. ', err);
  });
}

var nrpg = 0;
if ({{cookie - __sreff}}){
     nrpg = Number({{cookie - __sreff}}.split('.')[2]);
};
if (nrpg > 1 || {{Page URL}}.indexOf('aplicatii/extensie-chrome') != -1) {
    messaging.onTokenRefresh(function(){
      messaging.getToken().then(function(refreshedToken){
        console.log('Token refreshed.');
        resetUI();
      }).catch(function(err){
        console.log('Unable to retrieve refreshed token ', err);
      });
    });

    resetUI();

    var myOSTags = {};
    if (document.location.pathname.indexOf('servicii-fixe') != -1)
    {
      myOSTags.argoVisit = Math.floor(Date.now() / 1000);
    }
    if (typeof {{ecommerceDataLayer-Detail}} == "object" && typeof {{ecommerceDataLayer-Detail}}.products == "object" && typeof {{ecommerceDataLayer-Detail}}.products[0].brand != "undefined")
    {
      myOSTags[{{ecommerceDataLayer-Detail}}.products[0].brand] = "yes";
    }
    if ({{GeoLatitude}} != "na" && {{GeoLongitude}} != "na")
    {
      myOSTags.latitude = {{GeoLatitude}};
      myOSTags.longitude = {{GeoLongitude}};
    }
};
</script>
