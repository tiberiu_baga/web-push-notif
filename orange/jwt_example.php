<?php
require __DIR__ . '/vendor/autoload.php';
// Requires: composer require firebase/php-jwt
use Firebase\JWT\JWT;

$service_account_email = "new-service-account-key@push-notifications-c0fc0.iam.gserviceaccount.com";
$private_key = "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQC1muZSWZ3Kj2o8\n5QZh7jJmCgkg2w6Q0YJy7lhRo5RRQl1MD9fo9sF/akm0DkB6lE04B+wlw6RzfkmK\nIj6xx/cRkrIhNBVRnQx5blrqeno6/4bwe/gAYiOqP9WuJf4rbVrWejIojoZvhm3p\n3ifT7F9Guzu/o0aIt+rr2q0gJysSd+/yb1XfF9oy11FmMDaaQtLtBW7PBzOmoIIL\nT69l0RF8Da/bSw1Sqb7dxai4UOUrmp/RkbweB/JXDzHqtpvjdRlQsShC2mZ1EFLg\nQ6ynobpTd2RIUDUP8gmK1a0HQMlSKM5gPD5yrNgEHUPk30wlBCO1g2TNf7TpnUSQ\nEC03ASd9AgMBAAECggEAJGda5KaYuhZiGDdbvm1Kdohr+IVY5+BLm0DafPECxgA8\n5gamBAnU03cJs7mIpNnOGd2CsbZEZK9ry/Ad67LXPNNQeFXxsRS/f6cGvAqVrnZz\nBQmAtwj1N8dIjPqKNwfahfRkUhtx6PDEXzVce/+dwLa/HST8emMNyT4iZrfbSz3f\nnaJwtcf5/MtNKpYcmdR4w8MeA6wE9+j89qyR281Xbd6sU54aEwfNSEEosoCz+ndX\noyzIbfE8rpJyWMGVy0PJs/qK6BCimgN/b4kVN9noW//Gl6hWkIoUcoclREasBwpc\nJv8I53nJk6ZDy/Ba3CH8v4/ZB6NnNGmS6zEqOCW1QQKBgQDgMizw856mRxJ+NbBv\nL7GsPX4ZS2xQF7+8uzErzO7ap3T53EKnlaHyqU8GMC3fDvps9Z5qAubIkqrgV5Vy\nkBVAJw6Ou2wDXNRnCofPrT4aardYQSp1A9qhCSNIFM62PjCC8WynCItOerbr6y26\nNFY7dOOVH0/KrAT3NPOZyUhWvQKBgQDPXgHHnrAWHaiZc/JNg6eQTt4n7KqO6Oll\nYdxvN3xgsXhQxvxmUgb34MyIW7Cb26Q8ON5vF72R2UJtnydx4wme7nqXVa5gtGSF\nwjo4DMR1hdfrzuQddWEkDFZPRQ9kujyF7LNFCO2g/XNGJ+Iz5y6+uv+CDlDUtOfi\nPiXoJTB/wQKBgBlDbi8EtNb14U6ZjDUrQbEwCLqe9iicVH3W9jaCVFQNVLAnMteh\nk+s9SutemHyvOmb20pANnaXyaRD4rZ2IQOzGuew5kHVci47E8ALl+b6OUfS++yLO\neV92ISSpO5Xrshm/TMgRvWjtiRlSmgeGiEUQ6ehO6/XhpnmY3O5ycRVdAoGATTEl\nYHXF/rWTqWvr6sbzfViegiYrZ390QnUnZY82EZh+vTMVidFHHsbj3G6lU59zh6dz\nxc8ElmJVxBDW0iI91pfXsrYGb6E3OkOPAIZgK49jTTh62ixzuMOO7Xs7u7dICGKQ\naUZljuRKIPd/0OpiEEX0DyndFREysJmJ6tzNLQECgYBqaBEl+8KbeiwduEaa1xZD\n4EplXMMvWe8W9ohANi9zSAeuWis5qdzq8DkQpWxaX8BpSJRfo9RLCy9p6TvtWRL1\nKJ0qxSsXz1VrMiTznuk5wYD6Bm2nzDGTNP0/A8oeypteWKG5orxm9ACpNT6vTsRZ\nP9IK17QEbz6jjybk7ah4Hw==\n-----END PRIVATE KEY-----\n";

$token = create_custom_token('108811048120778961381', false);

function create_custom_token($uid, $is_premium_account)
{
    global $service_account_email, $private_key;

    $now_seconds = time();
    $payload = [
    "iss" => $service_account_email,
    "sub" => $service_account_email,
    "aud" => "https://identitytoolkit.googleapis.com/google.identity.identitytoolkit.v1.IdentityToolkit",
    "iat" => $now_seconds,
    "exp" => $now_seconds+(60*60),  // Maximum expiration time is one hour
    "uid" => $uid,
    "claims" => [
      "premium_account" => $is_premium_account
    ]
  ];
    return JWT::encode($payload, $private_key, "RS256");
}


$data_array = [
   "endpoint" => "https://fcm.googleapis.com/fcm/send/cRBZ8czLaAw:APA91bFHBpU8ffVM2SzthWue6s_lLzdBnDRT3Leviqv2ID4uYhLP9ysXJPdx9NhDihx6EDDaTYCZaX-LtaKo8YnXESMLi6udjab-SMxXj7yysjCG9NqCRWA3BOr9gqsB_tjpYu2jo7UR",
   "keys" => [
         "auth" => "I1mF4V7bg3mhteRyomiEzg==",
         "p256dh" => "BDvM2dL3hvJisUgeb3B3j8Q6SyXop3uBlRC9eMfggNfsVie4Jn5yIkOzAEiIwKbFfnkDa62My35ypE0tyghMGLg="
    ]
];

$make_call = callAPI('POST', 'https://iid.googleapis.com/v1/web/iid', json_encode($data_array), $token);
$response = json_decode($make_call, true);
print_r($response);
// $errors   = $response['response']['errors'];
// $data     = $response['response']['data'][0];



function callAPI($method, $url, $data, $token)
{
    $curl = curl_init();

    switch ($method) {
      case "POST":
         curl_setopt($curl, CURLOPT_POST, 1);
         if ($data) {
             curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
         }
         break;
      case "PUT":
         curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
         if ($data) {
             curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
         }
         break;
      default:
         if ($data) {
             $url = sprintf("%s?%s", $url, http_build_query($data));
         }
   }

    // OPTIONS:
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, [
        "Authorization: Bearer $token",
        'Crypto-Key: p256ecdsa=BHsJV2b0gGXNFbm_dhdXu8P-vDqi6615jXuzzlYOit66oIEiTIY_CFop68DBi8vTMoL3DQNAJBwIQRqFMdq4YHY',
        'Content-Type: application/json',
   ]);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

    // EXECUTE:
    $result = curl_exec($curl);
    if (!$result) {
        die("Connection Failure");
    }
    curl_close($curl);
    return $result;
}
