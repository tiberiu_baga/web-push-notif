    var buttonSelector = "#notification-button";

    function onManageWebPushSubscriptionButtonClicked(event) {
        if (window.Notification.permission == 'granted' || window.Notification.permission == 'denied') {
            return;
        }
        Notification.requestPermission().then((permission) => {
            updateMangeWebPushSubscriptionButton(buttonSelector);
            if (permission === 'granted') {
                resetUI();
            }
            resetUI();
        });
        event.preventDefault();
    }

    function updateMangeWebPushSubscriptionButton(buttonSelector) {
        if (!("Notification" in window)) {
          document.querySelector(buttonSelector).innerText = "Browser-ul tău nu permite notificări";
          document.querySelector(buttonSelector).classList.add('disabled');
          return;
        }

        var subscribeText = "Notificările sunt blocate. Pentru dezabonare consultați setările browserului.";
        var unsubscribeText = "Notificările sunt active. Pentru dezabonare consultați setările browserului.";
        var buttonStyle = 'disabled';
        var element = document.querySelector(buttonSelector);
        if (element === null) {
            return;
        }

        if (window.Notification.permission == 'granted') {
            var buttonText = unsubscribeText;
            element.classList.add('disabled');
        } else if (window.Notification.permission == 'denied'){
            var buttonText = subscribeText;
            element.classList.add('disabled');
        } else {
            var buttonText = "Nu ai activat notificările din browser. Activează!";
        }

        element.removeEventListener('click', onManageWebPushSubscriptionButtonClicked);
        element.addEventListener('click', onManageWebPushSubscriptionButtonClicked);
        element.textContent = buttonText;
    }

   updateMangeWebPushSubscriptionButton(buttonSelector);
