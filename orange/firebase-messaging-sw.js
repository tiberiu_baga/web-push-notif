importScripts('https://www.gstatic.com/firebasejs/8.0.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.0.0/firebase-auth.js');
importScripts('https://www.gstatic.com/firebasejs/8.0.0/firebase-messaging.js');
importScripts('https://www.gstatic.com/firebasejs/8.0.0/firebase-analytics.js');

var firebaseConfig = {
    apiKey: "AIzaSyB_glG1v9ctpnbwUgy1BV_ASUc03MEyPQQ",
    authDomain: "push-notifications-c0fc0.firebaseapp.com",
    databaseURL: "https://push-notifications-c0fc0.firebaseio.com",
    projectId: "push-notifications-c0fc0",
    storageBucket: "push-notifications-c0fc0.appspot.com",
    messagingSenderId: "819391863621",
    appId: "1:819391863621:web:76a324a2d9e26dbc5ebc2c",
    measurementId: "G-XMXPXHFSGK"
};
firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
    if ("notification" in payload) {
      payload.notification.requireInteraction = true;
      var myNotification = self.registration.showNotification(payload.notification.title, payload.notification);
    } else if("data" in payload) {
      var myNotification = self.registration.showNotification(payload.data.title, payload.data);
    }
});

self.addEventListener('notificationclick', function (event) {
    self.clients.openWindow(event.notification.data);
    event.notification.close();
})
