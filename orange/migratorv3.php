<?php
set_time_limit(60000);
echo "<br> Processing ...";
$timeStart = time();
$ch = curl_init();

// Connect to database
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "onesignal_users";
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT id, identifier, web_auth, web_p256 FROM onesignal_users WHERE ( processed = 0 OR result = 'Curl with no result') AND fcm_token IS NULL ORDER BY id DESC LIMIT 250";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while ($row = $result->fetch_assoc()) {
        list($token, $error) = convertToken($row['identifier'], $row['web_auth'], $row['web_p256']);
        if ($token) {
            $sql = "UPDATE onesignal_users SET processed = 1, fcm_token = '$token', result = NULL WHERE id = {$row['id']}";
            $conn->query($sql);
        } else {
            $sql = "UPDATE onesignal_users SET processed = 1, result = '$error' WHERE id = {$row['id']}";
            $conn->query($sql);
        }
    }
} else {
    echo "0 results";
}
$conn->close();


function convertToken($endpoint, $auth, $p256dh)
{
    global $ch;
    $url = 'https://iid.googleapis.com/v1/web/iid';
    $header = [
        'Content-Type:application/json',
        'Crypto-Key:p256ecdsa=BHsJV2b0gGXNFbm_dhdXu8P-vDqi6615jXuzzlYOit66oIEiTIY_CFop68DBi8vTMoL3DQNAJBwIQRqFMdq4YHY',
        'Authorization:key=AAAAvsePm0U:APA91bHKjC4ItbHX5DGMa2fB4mfnHHF-cxAJOUGzo2TSmkr9P8hhOnpMLdnbuwCEkdb-UiLQDtVk3ikWpVpM4M6QHs6qK7bM2dOgOS_4EwvaOCca1JK36jmh0b5JCVfDRBwGEjeSHkwY'
    ];
    $error =  false;
    $data = [
        "endpoint" => $endpoint,
        "keys" => [
             "auth" => $auth,
             "p256dh" => $p256dh
        ]
    ];

    $payload = json_encode($data);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    //return response instead of outputting
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);

    if (!$result) {
        return [false, "Curl with no result"];
    }
    $decoded = json_decode($result);
    if (!isset($decoded->token)) {
        return [
            false,
            isset($decoded->error->message) ? $decoded->error->message : "No token or message set"
        ];
    }
    return [$decoded->token, $error];
}

curl_close($ch);
$timeEnd = time();
echo "<br> Processtime = " . ($timeEnd-$timeStart) . " seconds";
echo "<br>DONE";
header("Refresh:0; url=migratorv3.php?time=" . date('H:i:s'));
// Orange token Tibi
// dbd64712-44f1-41bd-b1b6-85c854c6126e
