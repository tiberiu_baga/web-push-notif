var buttonSelector = "#notification-button";
if (!("Notification" in window)) {
  document.querySelector(buttonSelector).innerText = "Browser-ul tău nu permite notificări";
}
else {
   document.querySelector(buttonSelector).innerText = "Activează notificările";
};

function checkNotificationStatus() {
    if (!("Notification" in window)) {
        alert('No notifications!');
        return;
    }

    var subscribeText = "Activează notificările";
    var unsubscribeText = "Dezactivează notificările";
    if (window.Notification.permission == 'granted'){
        var buttonText = unsubscribeText;
    } else if (window.Notification.permission == 'denied'){
        var buttonText = unsubscribeText;
    } else {
        var buttonText = "Nu ai activat notificările din browser";
    }

    var element = document.querySelector(buttonSelector);
    if (element === null) {
        return;
    }

    element.removeEventListener('click', onManageWebPushSubscriptionButtonClicked);
    element.addEventListener('click', onManageWebPushSubscriptionButtonClicked);
    element.textContent = buttonText;
}

checkNotificationStatus();

 function onManageWebPushSubscriptionButtonClicked(event) {
    getSubscriptionState().then(function(state) {
        if (state.isPushEnabled) {
            / Subscribed, opt them out /
            deleteToken();
        } else {
            / Unsubscribed, subscribe them /
            requestPermission();
        }
    });
    event.preventDefault();
}


        var buttonSelector = "#notification-button";
        alert(window.Notification.permission);
        if (!("Notification" in window)) {
          document.querySelector(buttonSelector).innerText = "Browser-ul tău nu permite notificări";
        }
        else {
           document.querySelector(buttonSelector).innerText = "Activează notificările";
        };

        function getSubscriptionState() {
            var isPushEnabled = false;
            if (window.Notification.permission == 'granted') {
                var isPushEnabled = true;
            }

            return {
                isPushEnabled: isPushEnabled,
                isOptedOut: false
            };
        }



        function updateMangeWebPushSubscriptionButton(buttonSelector) {
            var subscribeText = "Activează notificările";
            var unsubscribeText = "Dezactivează notificările";

            getSubscriptionState().then(function(state) {
                if (window.Notification.permission=='granted'){
                    var buttonText = !state.isPushEnabled || state.isOptedOut ? subscribeText : unsubscribeText;
                } else {
                    var buttonText = "Nu ai activat notificările din browser";
                }
                alert(buttonText);

                var element = document.querySelector(buttonSelector);
                if (element === null) {
                    return;
                }

                element.removeEventListener('click', onManageWebPushSubscriptionButtonClicked);
                element.addEventListener('click', onManageWebPushSubscriptionButtonClicked);
                element.textContent = buttonText;
            });
        }

        updateMangeWebPushSubscriptionButton(buttonSelector);

        // OneSignal.on("subscriptionChange", function(isSubscribed) {
        //     updateMangeWebPushSubscriptionButton(buttonSelector);
        // });
        //
});
