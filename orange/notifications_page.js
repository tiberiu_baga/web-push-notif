    var buttonSelector = "#notification-button";
    if (!("Notification" in window)) {
      document.querySelector(buttonSelector).innerText = "Browser-ul tău nu permite notificări";
    }
    else {
       document.querySelector(buttonSelector).innerText = "Activează notificările";
    };

    function onManageWebPushSubscriptionButtonClicked(event) {
        getSubscriptionState().then(function(state) {
            if (state.isPushEnabled) {
                / Subscribed, opt them out /
                OneSignal.setSubscription(false);
            } else {
                if (state.isOptedOut) {
                    / Opted out, opt them back in /
                    OneSignal.setSubscription(true);
                } else {
                    / Unsubscribed, subscribe them /
                    OneSignal.registerForPushNotifications();
                }
            }
        });
        event.preventDefault();
    }

    function updateMangeWebPushSubscriptionButton(buttonSelector) {
        var subscribeText = "Activează notificările";
        var unsubscribeText = "Dezactivează notificările";

        getSubscriptionState().then(function(state) {

            if (window.Notification.permission=='granted'){
                var buttonText = !state.isPushEnabled || state.isOptedOut ? subscribeText : unsubscribeText;
            } else {
                var buttonText = "Nu ai activat notificările din browser";
            }
            //var buttonText = !state.isPushEnabled || state.isOptedOut ? subscribeText : unsubscribeText;

            var element = document.querySelector(buttonSelector);
            if (element === null) {
                return;
            }

            element.removeEventListener('click', onManageWebPushSubscriptionButtonClicked);
            element.addEventListener('click', onManageWebPushSubscriptionButtonClicked);
            element.textContent = buttonText;
        });
    }

    function getSubscriptionState() {
        return Promise.all([
          OneSignal.isPushNotificationsEnabled(),
          OneSignal.isOptedOut()
        ]).then(function(result) {
            var isPushEnabled = result[0];
            var isOptedOut = result[1];

            return {
                isPushEnabled: isPushEnabled,
                isOptedOut: isOptedOut
            };
        });
    }

    var OneSignal = OneSignal || [];

    / you've already initialized OneSignal /
    OneSignal.push(function() {
        // If we're on an unsupported browser, do nothing
        if (!OneSignal.isPushNotificationsSupported()) {
            return;
        }
        updateMangeWebPushSubscriptionButton(buttonSelector);
        OneSignal.on("subscriptionChange", function(isSubscribed) {
            / If the user's subscription state changes during the page's session, update the button text /
            updateMangeWebPushSubscriptionButton(buttonSelector);
        });
    });
