var fcmUserToken = '';
(function() {
    // if (window.location.host !== "www.yoxo.ro") {
    //     return;
    // }
    var fcmNotifPermissionRequested = localStorage.getItem('fcmNotifPermissionRequested');
    var firebaseConfig = {
    apiKey: "AIzaSyBDpoN5QscFukDzFayyAa-EZSxsFtjxlxM",
    authDomain: "innertrendswebpush.firebaseapp.com",
    databaseURL: "https://innertrendswebpush.firebaseio.com",
    projectId: "innertrendswebpush",
    storageBucket: "innertrendswebpush.appspot.com",
    messagingSenderId: "754622296668",
    appId: "1:754622296668:web:58f9ead1bacc08b0a36c99"
  };
    var publicVapidKey = 'BPtgvzWpAfZlxCSbI0pwMgQhkolbn9DzqjCpr0737PxflMAMRIjhhyGUNIO3Eyjx7djaeqIo7RyYnnpv5B3ts2Y';
    var trackPixelPath = '//yoxo.s3.eu-west-3.amazonaws.com/images/1pixel.png';

    firebase.initializeApp(firebaseConfig);
    var messaging = firebase.messaging();
    messaging.usePublicVapidKey(publicVapidKey);

    resetUI();

    messaging.onMessage(function(payload) {
        if ("notification" in payload) {
            console.table(payload.notification);
            payload.notification.requireInteraction = true;
            var myNotification = new Notification(payload.notification.title, payload.notification);

            myNotification.onclick = function(event) {
                event.preventDefault(); // prevent the browser from focusing the Notification's tab
                window.open(payload.notification.click_action, '_blank');
            }
        } else if ("data" in payload) {
            console.table(payload.data);
            var myNotification = new Notification(payload.data.title, payload.data);

            myNotification.onclick = function(event) {
                event.preventDefault(); // prevent the browser from focusing the Notification's tab
                window.open(payload.data.click_action, '_blank');
            }
        }
    });

    function resetUI() {
        if (Notification.permission == 'default') {
            fcmNotifPermissionRequested = 1;
            localStorage.setItem('fcmNotifPermissionRequested', 1);
        };

        messaging.getToken()
            .then(function(currentToken) {
                if (currentToken) {
                    fcmUserToken = currentToken;
                    if (fcmNotifPermissionRequested == 1) {
                        localStorage.setItem('fcmNotifPermissionRequested', null);
                        trackFcmAction(fcmUserToken, "registration", Math.floor(Date.now() / 1000));
                    } else {
                        trackFcmPageAction(fcmUserToken);
                    }
                } else {
                    requestPermission();
                }
            })
            .catch(function(err) {});
    }

    function requestPermission() {
        Notification.requestPermission()
            .then(function(permission) {
                if (permission === 'granted') {
                    localStorage.setItem('fcmNotifPermissionRequested', 1);
                    resetUI();
                }
            });
    }

    function getFcmToken() {
        return fcmUserToken;
    }

    function deleteToken() {
        messaging.getToken()
            .then(function(currentToken) {
                messaging.deleteToken(currentToken)
                    .then(function() {
                        resetUI();
                    })
                    .catch(function(err) {});
            })
            .catch(function(err) {});
    }

    function trackFcmPageAction(token) {
        var value = encodeURIComponent(window.location.href.replace(/(^\w+:|^)\/\//, ''));
        return trackFcmAction(token, "page", value);
    }

    function trackFcmAction(token, event, value) {
        var trackingPixel = new Image(1, 1);
        var pixelSrc = trackPixelPath +
            "?token=" + token +
            "&event=" + event +
            "&value=" + value +
            "&ord=" + (new Date())
            .getTime();
        trackingPixel.src = pixelSrc;
    }

    messaging.onTokenRefresh(function() {
        messaging.getToken()
            .then(function(refreshedToken) {
                resetUI();
            })
            .catch(function(err) {});
    });
})();
