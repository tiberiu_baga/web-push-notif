importScripts('https://www.gstatic.com/firebasejs/8.0.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.0.0/firebase-messaging.js');
importScripts('https://www.gstatic.com/firebasejs/8.0.0/firebase-analytics.js');

var firebaseConfig = {
    apiKey: "AIzaSyBDpoN5QscFukDzFayyAa-EZSxsFtjxlxM",
    authDomain: "innertrendswebpush.firebaseapp.com",
    databaseURL: "https://innertrendswebpush.firebaseio.com",
    projectId: "innertrendswebpush",
    storageBucket: "innertrendswebpush.appspot.com",
    messagingSenderId: "754622296668",
    appId: "1:754622296668:web:58f9ead1bacc08b0a36c99"
  };
firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
    if ("notification" in payload) {
      payload.notification.requireInteraction = true;
      var myNotification = self.registration.showNotification(payload.notification.title, payload.notification);
    } else if("data" in payload) {
      var myNotification = self.registration.showNotification(payload.data.title, payload.data);
    }
});

self.addEventListener('notificationclick', function (event) {
    self.clients.openWindow(event.notification.data);
    event.notification.close();
})
