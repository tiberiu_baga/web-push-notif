POST /fcm/send HTTP/1.1
Host: fcm.googleapis.com
Content-Type: application/json
Authorization: key=AAAAvsePm0U:APA91bHKjC4ItbHX5DGMa2fB4mfnHHF-cxAJOUGzo2TSmkr9P8hhOnpMLdnbuwCEkdb-UiLQDtVk3ikWpVpM4M6QHs6qK7bM2dOgOS_4EwvaOCca1JK36jmh0b5JCVfDRBwGEjeSHkwY
Cache-Control: no-cache
Postman-Token: b69f3b0f-fad7-a00d-fbba-4fc503312003

{
    "notification": {
        "title": "Dashrocks Inner Test",
        "body": "Firebase is awesome!!",
        "click_action": "http://dash.rocks",
        "icon": "http://dash.rocks/dash/asset/images/logo-sm.gif"
    },
    "to": "codcUkWQc_TZwy-yMSv1aQ:APA91bGJ_flHy-3Z-POiI4cMoADP-qGa5NkNEAjJi8-zGEsnV4F9GEPm2aAiE0K9yBdfWnf3dox78YyZyFHiels-_GjYRE80gZ0pJuDiNw5DidSi8BTATTe2Uz8rpFRtlFH_z0Rp-q2Y"
}
