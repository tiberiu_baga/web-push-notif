importScripts('https://www.gstatic.com/firebasejs/8.0.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.0.0/firebase-messaging.js');
importScripts('https://www.gstatic.com/firebasejs/8.0.0/firebase-analytics.js');

var firebaseConfig = {
    apiKey: "AIzaSyB9A7cNRHZGl34t9tzczq_3XtrPuuidbrk",
    authDomain: "yoxo-push-notifications.firebaseapp.com",
    databaseURL: "https://yoxo-push-notifications.firebaseio.com",
    projectId: "yoxo-push-notifications",
    storageBucket: "yoxo-push-notifications.appspot.com",
    messagingSenderId: "250966609505",
    appId: "1:250966609505:web:c9b0d310715f1e4cd1a420"
};
firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function(payload) {
    if ("notification" in payload) {
      payload.notification.requireInteraction = true;
      var myNotification = self.registration.showNotification(payload.notification.title, payload.notification);
    } else if("data" in payload) {
      var myNotification = self.registration.showNotification(payload.data.title, payload.data);
    }
});

self.addEventListener('notificationclick', function (event) {
    self.clients.openWindow(event.notification.data);
    event.notification.close();
})
