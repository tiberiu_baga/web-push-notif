var fcmUserToken = '';
(function() {
    // if (window.location.host !== "www.yoxo.ro") {
    //     return;
    // }
    var fcmNotifPermissionRequested = localStorage.getItem('fcmNotifPermissionRequested');
    var firebaseConfig = {
        apiKey: "AIzaSyB9A7cNRHZGl34t9tzczq_3XtrPuuidbrk",
        authDomain: "yoxo-push-notifications.firebaseapp.com",
        databaseURL: "https://yoxo-push-notifications.firebaseio.com",
        projectId: "yoxo-push-notifications",
        storageBucket: "yoxo-push-notifications.appspot.com",
        messagingSenderId: "250966609505",
        appId: "1:250966609505:web:c9b0d310715f1e4cd1a420"
    };
    var publicVapidKey = 'BJ8wADjxomCpHPm5SlauoEd0txK2adNJFR0Vw3T9UZepPRsIiqm_mgyIjv51eWql8dwqxaIBs4odrPRgkRD9Fs0';
    var trackPixelPath = '//yoxo.s3.eu-west-3.amazonaws.com/images/1pixel.png';

    firebase.initializeApp(firebaseConfig);
    var messaging = firebase.messaging();
    messaging.usePublicVapidKey(publicVapidKey);

    resetUI();

    messaging.onMessage(function(payload) {
        if ("notification" in payload) {
            payload.notification.requireInteraction = true;
            var myNotification = new Notification(payload.notification.title, payload.notification);
        } else if ("data" in payload) {
            var myNotification = new Notification(payload.data.title, payload.data);
        }
    });

    function resetUI() {
        if (Notification.permission == 'default') {
            fcmNotifPermissionRequested = 1;
            localStorage.setItem('fcmNotifPermissionRequested', 1);
        };

        messaging.getToken()
            .then(function(currentToken) {
                if (currentToken) {
                    fcmUserToken = currentToken;
                    if (fcmNotifPermissionRequested == 1) {
                        localStorage.setItem('fcmNotifPermissionRequested', null);
                        trackFcmAction(fcmUserToken, "registration", Math.floor(Date.now() / 1000));
                    } else {
                        trackFcmPageAction(fcmUserToken);
                    }
                } else {
                    requestPermission();
                }
            })
            .catch(function(err) {});
    }

    function requestPermission() {
        Notification.requestPermission()
            .then(function(permission) {
                if (permission === 'granted') {
                    localStorage.setItem('fcmNotifPermissionRequested', 1);
                    resetUI();
                }
            });
    }

    function getFcmToken() {
        return fcmUserToken;
    }

    function deleteToken() {
        messaging.getToken()
            .then(function(currentToken) {
                messaging.deleteToken(currentToken)
                    .then(function() {
                        resetUI();
                    })
                    .catch(function(err) {});
            })
            .catch(function(err) {});
    }

    function trackFcmPageAction(token) {
        var value = encodeURIComponent(window.location.href.replace(/(^\w+:|^)\/\//, ''));
        return trackFcmAction(token, "page", value);
    }

    function trackFcmAction(token, event, value) {
        var trackingPixel = new Image(1, 1);
        var pixelSrc = trackPixelPath +
            "?token=" + token +
            "&event=" + event +
            "&value=" + value +
            "&ord=" + (new Date())
            .getTime();
        trackingPixel.src = pixelSrc;
    }

    messaging.onTokenRefresh(function() {
        messaging.getToken()
            .then(function(refreshedToken) {
                resetUI();
            })
            .catch(function(err) {});
    });
})();
